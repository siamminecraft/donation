package me.suzutsuki.Bukkit.Donation;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import lib.PatPeter.SQLibrary.MySQL;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.logging.Logger;

/**
 * Created by Mizuki on 4/9/14.
 */
public class Donation extends JavaPlugin{

    public static Economy economy = null;
    public static Permission permission = null;
    public static WorldGuardPlugin worldGuardPlugin = null;
    public static Logger logger = Logger.getLogger("minecraft");
    public static String prefix = "[Donation]";
    public static String invalidnumber = "InvalidNumber";
    public static String invalidpoint = "InvalidPoint";
    public static String notenoughmoney = "NotEnoughMoney";
    public static String notenoughpoint = "NotEnoughPoint";
    public static String unexpectederror = "UnexpectedError";

    public static double rankbprice = 3000;
    public static String rankbtoken = "madoka";
    public static String rankbsuccessmsg = "Rank Up!";
    public static String rankbgroup = "Elite";

    public static String donatorgroup = "Donator";
    public static String donatortoken = "madoka";
    public static String donatorsuccessmsg = "Rank Up!";
    public static double donatorprice = 3000;

    private static String sqluser = "root";
    private static String sqlpassword = "";
    private static String sqldb = "bukkit_minecraft";
    private static String sqlhost = "localhost";
    private static int sqlport = 3306;

    public void onEnable()
    {
        if (!new File(getDataFolder(), "config.yml").exists()) {
            saveDefaultConfig();
        }
        setupEconomy();
        setupPermissions();
        reloadCfg();
        getCommand("rankbitem").setExecutor(new RankBCE(this));
        getCommand("rankb").setExecutor(new RankBCE(this));
        getCommand("donate").setExecutor(new DonationCE(this));
        getCommand("vipprotect").setExecutor(new DonationCE(this));
        getCommand("points").setExecutor(new DonationCE(this));
        getCommand("protect").setExecutor(new DonationCE(this));
        getCommand("pscrl").setExecutor(new DonationCE(this));
        getServer().getScheduler().scheduleSyncDelayedTask(this, new ExpireChecker(this));
        worldGuardPlugin = WGBukkit.getPlugin();

    }

    public void reloadCfg()
    {
        reloadConfig();
        rankbprice = getConfig().getDouble("rankb.price");
        rankbtoken = getConfig().getString("rankb.token");
        rankbsuccessmsg = getConfig().getString("rankb.successmsg");
        rankbgroup = getConfig().getString("rankb.group");
        sqluser = getConfig().getString("sql.user");
        sqlpassword = getConfig().getString("sql.password");
        sqldb = getConfig().getString("sql.db");
        sqlhost = getConfig().getString("sql.host");
        sqlport = getConfig().getInt("sql.port");
        prefix = getConfig().getString("message.prefix");
        notenoughpoint = getConfig().getString("message.notenoughpoint");
        unexpectederror = getConfig().getString("message.unexpectederror");
        notenoughmoney = getConfig().getString("message.notenoughmoney");
        invalidnumber = getConfig().getString("message.invalidnumber");
        invalidpoint = getConfig().getString("message.invalidpoint");
        donatorgroup = getConfig().getString("donator.group");
        donatortoken = getConfig().getString("donator.token");
        donatorsuccessmsg = getConfig().getString("donator.successmsg");
        donatorprice = getConfig().getDouble("donator.price");
    }
    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    public boolean onCommand(CommandSender cs,Command cmd,String label,String[] args)
    {
        if(cmd.getName().equalsIgnoreCase("donatereload"))
        {
            reloadCfg();
            cs.sendMessage("Donate Manager config reloaded.");
            return true;
        }
        if(cmd.getName().equalsIgnoreCase("rankb"))
        {
            cs.sendMessage("Executor not set");
            return true;
        }
        return false;
    }

    public static MySQL dbcon()
    {
        return new MySQL(Logger.getLogger("Minecraft"),"[Donation]",sqlhost,sqlport,sqldb,sqluser,sqlpassword);
    }
}
