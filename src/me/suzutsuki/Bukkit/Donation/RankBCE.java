package me.suzutsuki.Bukkit.Donation;

import me.suzutsuki.Bukkit.Donation.util.RankUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RankBCE implements CommandExecutor{

    Donation plugin = null;
    public RankBCE(Donation pl)
    {
        plugin=pl;
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String s, String[] args) {
        if(!(cs instanceof Player))
        {
            cs.sendMessage("This is player only command.");
            return true;
        }
        if(cmd.getName().equalsIgnoreCase("rankb"))
        {

            if(Donation.economy.getBalance(cs.getName()) >= plugin.rankbprice)
            {
                if(Donation.economy.withdrawPlayer(cs.getName(),plugin.rankbprice).transactionSuccess())
                {
                    //Donation.permission.playerAddGroup((Player)cs,plugin.rankbgroup);
                    RankUtil.getInstance().setRankB(cs.getName());
                    cs.sendMessage(plugin.rankbsuccessmsg);
                    return true;
                }
                else
                {
                    cs.sendMessage(Donation.notenoughmoney);
                }
            }
        }
        if(cmd.getName().equalsIgnoreCase("rankbitem"))
        {
            if(args.length == 1)
            {
                if(!args[0].equalsIgnoreCase(Donation.rankbtoken))
                {
                    cs.sendMessage("Invalid token.");
                    return true;
                }
                //Donation.permission.playerAddGroup((Player)cs,plugin.rankbgroup);
                RankUtil.getInstance().setRankB(cs.getName());
                cs.sendMessage(plugin.rankbsuccessmsg);
            }
        }
        return false;
    }
}
