package me.suzutsuki.Bukkit.Donation;

import me.suzutsuki.Bukkit.Donation.protection.ProtectionManager;
import me.suzutsuki.Bukkit.Donation.util.DBUtil;
import me.suzutsuki.Bukkit.Donation.util.ItemUtil;
import me.suzutsuki.Bukkit.Donation.util.PointUtil;
import me.suzutsuki.Bukkit.Donation.util.RankUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Mizuki on 4/9/14.
 */
public class DonationCE implements CommandExecutor{

    Donation plugin = null;
    public DonationCE(Donation pl)
    {
        plugin=pl;
    }
    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String s, String[] args) {
        if(!(cs instanceof Player))
        {
            cs.sendMessage("This is player only command.");
            return true;
        }
        if(cmd.getName().equalsIgnoreCase("donate"))
        {
            if(args.length != 1)
            {
                cs.sendMessage(Donation.prefix + "/donate <90,150,300,500>");
                return true;
            }
            if(args[0].equalsIgnoreCase("clear") && cs.hasPermission("donation.clear"))
            {
                cs.sendMessage(Donation.prefix + "กำลังล้าง Donator หมดอายุ");
                ExpireChecker.removeDonators();
                return true;
            }
            int amount = 0;
            try {
                amount = DBUtil.getInstance().getCurrentDonationAmount(cs.getName());
            } catch (SQLException e) {
                cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                e.printStackTrace();
                return true;
            }
            if(amount > 0 && amount != 112 )
            {
                cs.sendMessage(Donation.prefix + "Error : คุณเป็น Donator อยู่แล้ว , Code : " + Integer.toString(amount));
                return true;
            }
            else
            {
                int i = 0;
                try
                {
                    i = Integer.parseInt(args[0]);
                }
                catch(NumberFormatException e)
                {
                    cs.sendMessage(Donation.prefix + Donation.invalidnumber);
                  return true;
                }
                int point = 0;
                try
                {
                    point = PointUtil.getInstance().getPlayerPoint(cs.getName());
                } catch (SQLException e)
                {
                    cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                    e.printStackTrace();
                    return true;
                }
                if(point < i)
                {
                    cs.sendMessage(Donation.prefix + Donation.notenoughpoint);
                    return true;
                }
                int newpoint = point - i;
                if(newpoint > point)
                {
                    cs.sendMessage("You try to exploit? event logged.");
                    return true;
                }
                try
                {
                    Player p = (Player)cs;
                     switch (i)
                    {
                        case 90 :
                            DBUtil.getInstance().addDonationEntry(cs.getName(), 7,i);
                            RankUtil.getInstance().setDonator(cs.getName());
                            ItemStack[] stacks =
                                    {
                                        ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,2),
                                        ItemUtil.enchantedBook(Enchantment.DIG_SPEED,3),
                                        ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,3),
                                         new ItemStack(Material.EXP_BOTTLE,64),
                                         new ItemStack(Material.DIAMOND,6),
                                    };
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.newInstance().IRON_TOOL);
                            ItemUtil.giveItemFromArray(p.getInventory(),stacks);
                            ArrayList<String> lore = new ArrayList<String>();
                            lore.add("Protection Scroll");
                            lore.add("signed by Madoka");
                            p.getInventory().addItem(ItemUtil.loreItem(Material.PAPER,lore,"Protection Scroll 90"));
                            Donation.economy.depositPlayer(cs.getName(),900);
                            //Donation.permission.playerAdd(p,"mywarp.limit.packtiny");
                            RankUtil.getInstance().addPerm(cs.getName(), "mywarp.limit.packtiny");
                            RankUtil.getInstance().setPrefix(cs.getName(), "&f[&4A&f]");
                            break;
                        case 150 :
                            DBUtil.getInstance().addDonationEntry(cs.getName(), 13,i);
                            RankUtil.getInstance().setDonator(cs.getName());
                            ItemStack[] itemStacks =
                                    {
                                            ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,3),
                                            ItemUtil.enchantedBook(Enchantment.DIG_SPEED,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.SILK_TOUCH,1),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.DIAMOND,9)
                                    };
                            ItemUtil.giveItemFromArray(p.getInventory(),itemStacks);
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.newInstance().IRON_TOOL);
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.newInstance().IRON_ARMOR);
                            Donation.economy.depositPlayer(cs.getName(),1500);
                            ArrayList<String> loree = new ArrayList<String>();
                            loree.add("Protection Scroll");
                            loree.add("signed by Madoka");
                            p.getInventory().addItem(ItemUtil.loreItem(Material.PAPER,loree,"Protection Scroll 150"));
                            //Donation.permission.playerAdd(p,"mywarp.limit.packsmall");
                            RankUtil.getInstance().addPerm(cs.getName(), "mywarp.limit.packsmall");
                            RankUtil.getInstance().setPrefix(cs.getName(), "&f[&4AA&f]");
                            break;
                        case 300 :
                            DBUtil.getInstance().addDonationEntry(cs.getName(), 28,i);
                            RankUtil.getInstance().setDonator(cs.getName());
                            ItemStack[] items =
                                    {
                                            ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,3),
                                            ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,3),
                                            ItemUtil.enchantedBook(Enchantment.DIG_SPEED,4),
                                            ItemUtil.enchantedBook(Enchantment.DIG_SPEED,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.SILK_TOUCH,1),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,8),
                                            new ItemStack(Material.DIAMOND,20)
                                    };
                            ItemUtil.giveItemFromArray(p.getInventory(),items);
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.IRON_TOOL);
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.CHAIN_ARMOR);
                            Donation.economy.depositPlayer(cs.getName(),3000);
                            ArrayList<String> loreee = new ArrayList<String>();
                            loreee.add("Protection Scroll");
                            loreee.add("signed by Madoka");
                            p.getInventory().addItem(ItemUtil.loreItem(Material.PAPER,loreee,"Protection Scroll 300"));
                            //Donation.permission.playerAdd(p,"mywarp.limit.packmedium");
                            RankUtil.getInstance().addPerm(cs.getName(), "mywarp.limit.packmedium");
                            //Donation.permission.playerAdd(p,"essentials.enderchest");
                            RankUtil.getInstance().addPerm(cs.getName(), "essentials.enderchest");
                            RankUtil.getInstance().setPrefix(cs.getName(), "&f[&eS&f]");
                            break;
                        case 500 :
                            DBUtil.getInstance().addDonationEntry(cs.getName(), 55,i);
                            RankUtil.getInstance().setDonator(cs.getName());
                            Donation.economy.depositPlayer(cs.getName(),6000);
                            ItemStack[] itemst =
                                    {
                                            ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,3),
                                            ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,3),
                                            ItemUtil.enchantedBook(Enchantment.LOOT_BONUS_BLOCKS,3),
                                            ItemUtil.enchantedBook(Enchantment.DIG_SPEED,4),
                                            ItemUtil.enchantedBook(Enchantment.DIG_SPEED,4),
                                            ItemUtil.enchantedBook(Enchantment.DIG_SPEED,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.PROTECTION_ENVIRONMENTAL,4),
                                            ItemUtil.enchantedBook(Enchantment.SILK_TOUCH,1),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.EXP_BOTTLE,64),
                                            new ItemStack(Material.DIAMOND,40),
                                            new ItemStack(Material.BEACON)
                                    };
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.DIAMOND_TOOL);
                            ItemUtil.giveItemFromArray(p.getInventory(),ItemUtil.DIAMOND_ARMOR);
                            ItemUtil.giveItemFromArray(p.getInventory(),itemst);
                            ArrayList<String> lores = new ArrayList<String>();
                            lores.add("Protection Scroll");
                            lores.add("signed by Madoka");
                            p.getInventory().addItem(ItemUtil.loreItem(Material.PAPER,lores,"Protection Scroll 500"));
                            //Donation.permission.playerAdd(p, "mywarp.limit.packbig");
                            RankUtil.getInstance().addPerm(cs.getName(), "mywarp.limit.packbig");
                            //Donation.permission.playerAdd(p,"essentials.enderchest");
                            RankUtil.getInstance().addPerm(cs.getName(), "essentials.enderchest");
                            RankUtil.getInstance().setPrefix(cs.getName(), "&f[&eSS&f]");
                            RankUtil.getInstance().setSuffix(cs.getName(), "&f[&6Exclusive&f]");
                            break;
                        default:
                            cs.sendMessage(Donation.prefix + Donation.invalidpoint);
                            return true;
                    }
                }
                catch(SQLException ex)
                {
                    cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                    ex.printStackTrace();
                    return true;
                }
                try
                {
                    PointUtil.getInstance().setPlayerPoint(cs.getName(),newpoint);
                } catch (SQLException e)
                {
                    cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                    e.printStackTrace();
                    return true;
                }
                cs.sendMessage(Donation.donatorsuccessmsg);
                try {
                    DBUtil.getInstance().logDonation(cs.getName(),i);
                } catch (SQLException e) {
                    cs.sendMessage("Error logging donation");
                    e.printStackTrace();
                }
            }
        }
        if(cmd.getName().equalsIgnoreCase("points"))
        {
            int point = 0;
            try
            {
                point = PointUtil.getInstance().getPlayerPoint(cs.getName());
                cs.sendMessage(Donation.prefix + "คุณมีแต้ม Donate อยู่ " + Integer.toString(point));
                return true;
            } catch (SQLException e)
            {
                cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                e.printStackTrace();
                return true;
            }
        }
        if(cmd.getName().equalsIgnoreCase("pscrl"))
        {
            if(args.length != 2)
            {
                //TODO Help for protect
                cs.sendMessage("/pscrl add <ชื่อผู้เล่น> - เพิ่มผู้เล่นเข้าโพรเทค");
                cs.sendMessage("/pscrl remove <ชื่อผู้เล่น> - ลบผู้เล่นออกจากโพรเทค");
                return true;
            }
            int i = 0;
            if(args[0].equalsIgnoreCase("add"))
            {
                i = ProtectionManager.getInstance().addMember((Player) cs, args[1]);
                if(i > 0)cs.sendMessage(Donation.prefix + "เพิ่มผู้เล่นเข้าโพรเทคที่เป็นของคุณเรียบร้อยแล้ว");
                else cs.sendMessage(Donation.prefix + "ไม่พบโพรเทคของคุณ");
                return true;
            }
            if(args[0].equalsIgnoreCase("remove"))
            {
                i = ProtectionManager.getInstance().removeMember((Player) cs,args[1]);
                if(i > 0)cs.sendMessage(Donation.prefix + "ลบผู้เล่นออกจากโพรเทคที่เป็นของคุณเรียบร้อยแล้ว");
                else cs.sendMessage(Donation.prefix + "ไม่พบโพรเทคของคุณ");
                return true;
            }
        }
        if(cmd.getName().equalsIgnoreCase("protect"))
        {
            if(args.length != 1)
            {
                cs.sendMessage(Donation.prefix + "/protect <150,300,500> - โพรเทคพื้นที่ที่คุณยืนอยู่ทันที");
                return true;
            }
            if(args.length == 1)
            {
                int price = 0;
                try
                {
                    price = Integer.parseInt(args[0]);
                }
                catch(NumberFormatException e)
                {
                    cs.sendMessage(Donation.prefix + Donation.invalidnumber);
                    return true;
                }
                int point = 0;
                try
                {
                    point = PointUtil.getInstance().getPlayerPoint(cs.getName());
                } catch (SQLException e)
                {
                    cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                    e.printStackTrace();
                    return true;
                }
                if(point < price)
                {
                    cs.sendMessage(Donation.prefix + Donation.notenoughpoint);
                    return true;
                }
                int newpoint = point - price;
                if(newpoint > point)
                {
                    cs.sendMessage("You try to exploit? event logged.");
                    return true;
                }
                Player p = (Player)cs;
                boolean success = true;
                if(price == 150)
                {
                    success = ProtectionManager.getInstance().createRegion(p, 50);
                }
                else if(price == 300)
                {
                    success = ProtectionManager.getInstance().createRegion(p, 100);
                }
                else if(price == 500)
                {
                    success = ProtectionManager.getInstance().createRegion(p, 200);
                }
                else
                {
                    cs.sendMessage(Donation.prefix + "/protect <150,300,500> - โพรเทคพื้นที่ที่คุณยืนอยู่ทันที");
                    return true;
                }
                if(success)cs.sendMessage(Donation.prefix + "สร้างโพรเทคเรียบร้อย");
                else
                {
                    cs.sendMessage(Donation.prefix + "ไม่สามารถสร้างโพรเทค อาจเกิดจากพื้นที่ที่จะสร้างทับกับโพรเทคอื่น");
                }
                try
                {
                    PointUtil.getInstance().setPlayerPoint(cs.getName(),newpoint);
                } catch (SQLException e)
                {
                    cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                    e.printStackTrace();
                    return true;
                }
                return true;
            }
        }
        if(cmd.getName().equalsIgnoreCase("vipprotect"))
        {
            int amount = 0;
            try {
                amount = DBUtil.getInstance().getCurrentDonationAmount(cs.getName());
            } catch (SQLException e) {
                cs.sendMessage(Donation.prefix + Donation.unexpectederror);
                e.printStackTrace();
                return true;
            }
            ProtectionManager pm = ProtectionManager.getInstance();
            Player p = (Player)cs;
            boolean success = false;
            if(amount == 90)
            {
                success = pm.createVIPRegion(p, 30);
            }
            else if(amount == 150)
            {
                success = pm.createVIPRegion(p, 50);
            }
            else if(amount == 300)
            {
                success = pm.createVIPRegion(p, 70);
            }
            else if(amount == 500)
            {
                success = pm.createVIPRegion(p, 100);
            }
            else
            {
                cs.sendMessage(Donation.prefix + "คุณไม่ใช่ Donator หรือข้อมูลมีความผิดพลาด (" + Integer.toString(amount) + ")");
                return true;
            }
            if(success)cs.sendMessage(Donation.prefix + "สร้างโพรเทคเรียบร้อย");
            else
            {
                cs.sendMessage(Donation.prefix + "ไม่สามารถสร้างโพรเทค อาจเกิดจากพื้นที่ที่จะสร้างทับกับโพรเทคอื่น");
            }
            return true;
        }
        return false;
    }

    public void showHelp(CommandSender cs)
    {

    }
}
