package me.suzutsuki.Bukkit.Donation.protection;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.RegionGroup;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.suzutsuki.Bukkit.Donation.Donation;
import me.suzutsuki.Bukkit.Donation.util.DBUtil;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

/**
 * Created by Mizuki on 4/12/14.
 */
public class ProtectionManager {

    private static ProtectionManager pm = null;
    protected ProtectionManager(){}

    public static ProtectionManager getInstance()
    {
        if(pm == null)
        {
            pm = new ProtectionManager();
        }
        return pm;
    }
    public boolean createVIPRegion(Player player,int radius,boolean allowMoveRegion)
    {
		radius = (int) Math.floor(radius/2);
        RegionManager rm = WGBukkit.getRegionManager(player.getWorld());
        Location loc = player.getLocation();
        BlockVector b1 = new BlockVector(loc.getX() + radius + 1, 0, loc.getZ() + radius + 1);
        BlockVector b2 = new BlockVector(loc.getX() - radius - 1, 255, loc.getZ() - radius - 1);
        String msec = "undefined";
        try {
            msec = Long.toString(new Date(DBUtil.getInstance().getExpire(player.getName()).getTime()).getTime());
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        if(rm.getRegion(player.getName() + "_" + msec) != null)
        {
            if(allowMoveRegion)
            {
                rm.removeRegion(player.getName() + "_" + msec);
            }
            else
            {
                return false;
            }
        }
        ProtectedCuboidRegion pc = new ProtectedCuboidRegion(player.getName() + "_" + msec,b1,b2);
        DefaultDomain dd = new DefaultDomain();
        dd.addPlayer(player.getName());
        pc.setOwners(dd);
        if(rm.overlapsUnownedRegion(pc,Donation.worldGuardPlugin.wrapPlayer(player)))
        {
            return false;
        }
        pc.setFlag(new StringFlag("greeting", RegionGroup.ALL),"เข้าสู่พื้นที่ของ " + player.getName());
        rm.addRegion(pc);
        try
        {
            rm.save();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    public boolean createVIPRegion(Player player,int radius)
    {
        return createVIPRegion(player, radius, false);
    }
    public boolean createRegion(Player player,int radius)
    {
        radius = (int) Math.floor(radius/2);
        RegionManager rm = WGBukkit.getRegionManager(player.getWorld());
        Location loc = player.getLocation();
        BlockVector b1 = new BlockVector(loc.getX() + radius + 1, 0, loc.getZ() + radius + 1);
        BlockVector b2 = new BlockVector(loc.getX() - radius - 1, 255, loc.getZ() - radius - 1);
        String random = Integer.toHexString(new Random().nextInt(Integer.MAX_VALUE));
        ProtectedCuboidRegion pc = new ProtectedCuboidRegion("permpt_" + player.getName() + "_" + random,b1,b2);
        DefaultDomain dd = new DefaultDomain();
        dd.addPlayer(player.getName());
        pc.setOwners(dd);
        if(rm.overlapsUnownedRegion(pc,Donation.worldGuardPlugin.wrapPlayer(player)))
        {
            return false;
        }
        pc.setFlag(new StringFlag("greeting", RegionGroup.ALL),"เข้าสู่พื้นที่ของ " + player.getName());
        rm.addRegion(pc);
        try
        {
            rm.save();
            return true;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    public int addMember(Player owner,String toadd)
    {
        RegionManager rm = WGBukkit.getRegionManager(owner.getWorld());
        Location loc = owner.getLocation();
        ApplicableRegionSet ars = rm.getApplicableRegions(loc);
        int i = 0;
        for(ProtectedRegion pr : ars)
        {
            if(pr.getId().toLowerCase().startsWith(owner.getName().toLowerCase()))
            {
                DefaultDomain dd = pr.getMembers();
                dd.addPlayer(toadd);
                pr.setMembers(dd);
                i++;
            }
            if(pr.getId().toLowerCase().startsWith("permpt_" + owner.getName().toLowerCase()))
            {
                DefaultDomain dd = pr.getMembers();
                dd.addPlayer(toadd);
                pr.setMembers(dd);
                i++;
            }
        }
        return i;
    }
    public int removeMember(Player owner,String toremove)
    {
        RegionManager rm = WGBukkit.getRegionManager(owner.getWorld());
        Location loc = owner.getLocation();
        ApplicableRegionSet ars = rm.getApplicableRegions(loc);
        int i = 0;
        for(ProtectedRegion pr : ars)
        {
            if(pr.getId().startsWith(owner.getName()))
            {
                DefaultDomain dd = pr.getMembers();
                dd.removePlayer(toremove);
                pr.setMembers(dd);
                i++;
            }
            if(pr.getId().toLowerCase().startsWith("permpt_" + owner.getName().toLowerCase()))
            {
                DefaultDomain dd = pr.getMembers();
                dd.removePlayer(toremove);
                pr.setMembers(dd);
                i++;
            }
        }
        return i;
    }
}
