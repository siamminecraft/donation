package me.suzutsuki.Bukkit.Donation;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.databases.ProtectionDatabaseException;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.suzutsuki.Bukkit.Donation.util.DBUtil;
import me.suzutsuki.Bukkit.Donation.util.RankUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Mizuki on 4/10/14.
 */
public class ExpireChecker implements Runnable {

    Donation pl = null;
    public ExpireChecker(Donation pl)
    {
        this.pl=pl;
    }
    public void run()
    {
        removeDonators();
        Bukkit.getScheduler().scheduleSyncDelayedTask(pl,this,72000);
    }
    public static void removeDonators()
    {
        List<World> worlds = Bukkit.getWorlds();
        List<String> donors = null;
        Long currenttime = new Date().getTime();
        try {
            donors = DBUtil.getInstance().getDonators();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(donors == null)
        {
            Donation.logger.warning("Cannot get donator list.");
            return;
        }
        for(World world : worlds)
        {
            RegionManager regionManager = WGBukkit.getRegionManager(world);
            Map<String, ProtectedRegion> regionMap = regionManager.getRegions();
            for(ProtectedRegion pr : regionMap.values())
            {
                String[] info = pr.getId().split("_");
                if(info.length == 2 && donors.contains(info[0]))
                {
                    Donation.logger.info("Found VIP region " + pr.getId());
                    String owner = info[0];
                    String expire = info[1];
                    if(donors.contains(owner))
                    {
                        if(Long.parseLong(expire) < currenttime)
                        {
                            regionManager.removeRegion(pr.getId());
                        }
                    }
                }
            }
            try {
                regionManager.save();
            } catch (ProtectionDatabaseException e) {
                e.printStackTrace();
                Donation.logger.warning("Cannot save RegionManager.");
            }
        }
        RankUtil ru = RankUtil.getInstance();
        DBUtil du = DBUtil.getInstance();
        for(String player : donors)
        {
            Donation.logger.info("Trying to check expire status of " + player);
            try {
                if(DBUtil.getInstance().getExpire(player).getTime() < currenttime)
                {
                    Bukkit.getServer().broadcastMessage(ChatColor.RED + Donation.prefix + "กำลังล้างสถานะ Donator ของ " + player);
                    du.removeDonationEntry(player);
                    ru.setRankB(player);
                    ru.setPrefix(player,"\"\"");
                    ru.removePerm(player,"essentials.enderchest");
                    ru.removePerm(player,"mywarp.limit.packtiny");
                    ru.removePerm(player,"mywarp.limit.packsmall");
                    ru.removePerm(player,"mywarp.limit.packmedium");
                    ru.removePerm(player,"mywarp.limit.packbig");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                Donation.logger.warning("Cannot check expire time.");
            }
        }
    }
}
