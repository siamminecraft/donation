package me.suzutsuki.Bukkit.Donation.util;

import lib.PatPeter.SQLibrary.MySQL;
import me.suzutsuki.Bukkit.Donation.Donation;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Mizuki on 4/10/14.
 */
public class DBUtil {
    private static DBUtil instance = null;

    protected DBUtil(){}

    public static DBUtil getInstance()
    {
        if(instance == null)
        {
            instance = new DBUtil();
        }
        return instance;
    }

    public void addDonationEntry(String name,int expirein,int amount) throws SQLException {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, expirein);
        Timestamp expire = new Timestamp(c.getTimeInMillis());
        PreparedStatement stmt = con.prepare("INSERT INTO player_donation (username,start,expire,amount) VALUES (?,CURRENT_TIMESTAMP,?,?)");
        stmt.setString(1,name);
        stmt.setTimestamp(2, expire);
        stmt.setInt(3,amount);
        stmt.executeUpdate();
    }

    public void removeDonationEntry(String name) throws SQLException {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        PreparedStatement stmt = con.prepare("DELETE FROM player_donation WHERE username LIKE ?");
        stmt.setString(1, name);
        stmt.executeUpdate();
    }
    public void logDonation(String name,int amount) throws SQLException {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        PreparedStatement stmt = con.prepare("INSERT INTO donationlog (username,time,amount) VALUES (?,CURRENT_TIMESTAMP,?)");
        stmt.setString(1,name);
        stmt.setInt(2,amount);
        stmt.executeUpdate();
    }

    public ResultSet getPlayerDonateInfo(String name) throws SQLException {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        PreparedStatement stmt = con.prepare("SELECT * FROM player_donation WHERE username LIKE ?");
        stmt.setString(1,name);
        ResultSet rs = stmt.executeQuery();
        return rs;
    }
    public Timestamp getExpire(String name) throws SQLException {
        ResultSet rs = getPlayerDonateInfo(name);
        Timestamp ts = null;
        while(rs.next())
        {
            ts = rs.getTimestamp("expire");
        }
        return ts;
    }

    public int getCurrentDonationAmount(String name) throws SQLException
    {
        ResultSet rs = getPlayerDonateInfo(name);
        int amount = 112;
        while (rs.next())
        {
            int i = rs.getInt("amount");
            Donation.logger.info(Integer.toString(i));
            amount = i;
        }
        return amount;
    }

    public List<String> getDonators() throws SQLException
    {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        ResultSet rs = con.query("SELECT * FROM player_donation");
        ArrayList<String> donors = new ArrayList<String>();
        while(rs.next())
        {
            donors.add(rs.getString("username"));
        }
        return (List)donors;
    }
}
