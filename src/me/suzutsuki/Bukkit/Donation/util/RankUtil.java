package me.suzutsuki.Bukkit.Donation.util;

import me.suzutsuki.Bukkit.Donation.Donation;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Mizuki on 4/10/14.
 */
public class RankUtil {

    private static RankUtil instance = null;

    protected RankUtil(){}

    public static RankUtil getInstance()
    {
        if(instance == null)
        {
            instance = new RankUtil();
        }
        return instance;
    }

    public void setRankB(Player p)
    {
        setRankB(p.getName());
    }

    public void setRankB(String name)
    {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"pex user " + name + " group set " + Donation.rankbgroup);
    }

    public void setDonator(Player p)
    {
        setDonator(p.getName());
    }
    public void setDonator(String name)
    {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"pex user " + name + " group set " + Donation.donatorgroup);
    }
    public void setPrefix(String name, String prefix)
    {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"pex user "+ name + " prefix " + prefix);
    }
    public void setSuffix(String name, String prefix)
    {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"pex user "+ name + " suffix " + prefix);
    }
    public void addPerm(String name, String perm)
    {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"pex user "+ name + " add " + perm);
    }
    public void removePerm(String name, String perm)
    {
        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(),"pex user "+ name + " remove " + perm);
    }

}
