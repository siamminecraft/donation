package me.suzutsuki.Bukkit.Donation.util;

import lib.PatPeter.SQLibrary.MySQL;
import me.suzutsuki.Bukkit.Donation.Donation;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Mizuki on 4/10/14.
 */
public class PointUtil {

    private static PointUtil instance = null;

    protected PointUtil(){}

    public static PointUtil getInstance()
    {
        if(instance==null)
        {
            instance = new PointUtil();
        }
        return instance;
    }

    public int getPlayerPoint(Player p) throws SQLException {
        return getPlayerPoint(p.getName());
    }
    public int getPlayerPoint(String name) throws SQLException {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        PreparedStatement pstmt = con.prepare("SELECT COUNT(*) FROM player_points WHERE username = ?");
        pstmt.setString(1,name);
        ResultSet rs = pstmt.executeQuery();
        int rows = 0;
        while(rs.next())
        {
            rows = rs.getInt(1);
        }
        if(rows > 0)
        {
            pstmt = null;
            pstmt = con.prepare("SELECT points FROM player_points WHERE username = ?");
            pstmt.setString(1,name);
            rs = null;
            rs = pstmt.executeQuery();
            while(rs.next())
            {
                return rs.getInt(1);
            }
            return 0;
        }
        else
        {
            return 0;
        }
    }
    public void setPlayerPoint(String name,int point) throws SQLException {
        MySQL con = Donation.dbcon();
        if(!con.isOpen())
        {
            con.open();
        }
        PreparedStatement pstmt = con.prepare("UPDATE player_points SET points = ? WHERE username = ?");
        pstmt.setInt(1,point);
        pstmt.setString(2,name);
        pstmt.executeUpdate();
    }

    public void setPlayerPoint(Player p,int point) throws SQLException {
        setPlayerPoint(p.getName(),point);
    }
}
