package me.suzutsuki.Bukkit.Donation.util;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by Mizuki on 4/10/14.
 */
public class ItemUtil {

    public static ItemStack[] IRON_ARMOR = { new ItemStack(Material.IRON_CHESTPLATE) , new ItemStack(Material.IRON_LEGGINGS) , new ItemStack(Material.IRON_BOOTS) , new ItemStack(Material.IRON_HELMET)};
    public static ItemStack[] IRON_TOOL = { new ItemStack(Material.IRON_SWORD) , new ItemStack(Material.IRON_SPADE) , new ItemStack(Material.IRON_AXE) , new ItemStack(Material.IRON_PICKAXE) , new ItemStack(Material.IRON_HOE)};
    public static ItemStack[] DIAMOND_ARMOR = { new ItemStack(Material.DIAMOND_CHESTPLATE) , new ItemStack(Material.DIAMOND_LEGGINGS) , new ItemStack(Material.DIAMOND_BOOTS) , new ItemStack(Material.DIAMOND_HELMET)};
    public static ItemStack[] DIAMOND_TOOL = { new ItemStack(Material.DIAMOND_SWORD) , new ItemStack(Material.DIAMOND_SPADE) , new ItemStack(Material.DIAMOND_AXE) , new ItemStack(Material.DIAMOND_PICKAXE) , new ItemStack(Material.DIAMOND_HOE)};
    public static ItemStack[] CHAIN_ARMOR = {new ItemStack(Material.CHAINMAIL_BOOTS) , new ItemStack(Material.CHAINMAIL_CHESTPLATE) , new ItemStack(Material.CHAINMAIL_LEGGINGS) , new ItemStack(Material.CHAINMAIL_HELMET)};
    public static ItemStack enchantedBook(Enchantment enc,int level)
    {
        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta bookmeta = (EnchantmentStorageMeta) book.getItemMeta();
        bookmeta.addStoredEnchant(enc,level,true);
        book.setItemMeta(bookmeta);
        return book;
    }
    public static void giveItemFromArray(Inventory inv,ItemStack[] stacks)
    {
        for(ItemStack stack : stacks)
        {
            inv.addItem(stack);
        }
    }
    public static ItemUtil newInstance()
    {
        return new ItemUtil();
    }
    public static ItemStack loreItem(Material material,List<String> lore,String name)
    {
        ItemStack stack = new ItemStack(material);
        ItemMeta meta = stack.getItemMeta();
        meta.setLore(lore);
        meta.setDisplayName(name);
        stack.setItemMeta(meta);
        return stack;
    }
}
